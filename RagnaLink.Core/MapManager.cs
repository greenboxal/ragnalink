﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RagnaLink.Core.Algorithms;
using RagnaLink.Core.Ragnarok;

namespace RagnaLink.Core
{
    public class MapManager
    {
        private MapCache _mapCache;
        public MapCache MapCache
        {
            set { _mapCache = value; }
        }

        private Hashtable _maps;
        public Hashtable Maps
        {
            get { return _maps; }
        }

        public MapManager()
        {
            _mapCache = new MapCache();
            _maps = new Hashtable();
        }

        public void Load()
        {
            LogManager.Instance.AppendLine("[MapManager] Loading mapcache...");

            FileStream fs = new FileStream("data/mapcache.dat", FileMode.Open);
            _mapCache.Load(fs);
            fs.Close();

            LogManager.Instance.AppendLine("[MapManager] Loading {0} maps...", _mapCache.Maps.Count);

            _maps = new Hashtable();
            foreach (DictionaryEntry entry in _mapCache.Maps)
            {
                MapDescriptor desc = new MapDescriptor();
                MapData map = (MapData)entry.Value;

                LogManager.Instance.LastLine = string.Format("[MapManager] Loading map {0}...", map.Name);

                desc.Map = map;

                /*
                desc.Finder = new PathFinder(map);
                desc.Finder.Heuristic = PathFinderHeuristic.EuclideanDistance;
                 */

                desc.Finder = new AStartPathFinder(map);

                _maps.Add(map.Name.ToLower(), desc);
            }
            LogManager.Instance.LastLine += " Done.";
            LogManager.Instance.AppendLine("[MapManager] {0} maps lodaded.", _maps.Count);
        }

        public MapDescriptor GetMap(string name)
        {
            if (name.EndsWith(".gat"))
                name = name.Substring(0, name.Length - 4);

            if (_maps.ContainsKey(name))
                return (MapDescriptor)_maps[name];

            return null;
        }
    }

    public class MapDescriptor
    {
        public IPathFinder Finder { get; set; }
        public MapData Map { get; set; }
    }
}
