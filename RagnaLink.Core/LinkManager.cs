﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using RagnaLink.Core.Algorithms;
using RagnaLink.Core.Plugin;
using RagnaLink.Core.Proxy;
using RagnaLink.Core.Ragnarok;
using RagnaLink.Core.Ragnarok.Network;

namespace RagnaLink.Core
{
    public class LinkManager
    {
        public static LinkManager Instance { get; set; }

        public const string CommandPrefix = "#";

        static LinkManager()
        {
            Instance = new LinkManager();
        }

        private int _portCounter;

        private RagProxy _loginProxy;
        public RagProxy LoginProxy
        {
            get { return _loginProxy; }
        }

        private LinkConfig _config;
        public LinkConfig Config
        {
            get { return _config; }
        }

        private Dictionary<IPEndPoint, RagProxy> _servers;
        public Dictionary<IPEndPoint, RagProxy> Servers
        {
            get { return _servers; }
        }

        private Dictionary<int, RagCookie> _cookies;
        public Dictionary<int, RagCookie> Cookies
        {
            get { return _cookies; }
        }

        private PluginLoader _pluginLoader;
        public PluginLoader PluginLoader
        {
            get { return _pluginLoader; }
        }

        private List<IZoneHandler> _zoneHandlers;
        public List<IZoneHandler> ZoneHandlers
        {
            get { return _zoneHandlers; }
        }

        private Hashtable _zoneCommands;
        public Hashtable ZoneCommands
        {
            get { return _zoneCommands; }
        }

        private MapManager _maps;
        public MapManager Maps
        {
            get { return _maps; }
        }

        private LinkManager()
        {
            _config = new LinkConfig();
            _servers = new Dictionary<IPEndPoint, RagProxy>();
            _cookies = new Dictionary<int, RagCookie>();
            _pluginLoader = new PluginLoader();
            _zoneHandlers = new List<IZoneHandler>();
            _zoneCommands = new Hashtable();
            _maps = new MapManager();

            _portCounter = _config.ListenPortBase;
        }

        public void Start()
        {
            _maps.Load();

            _pluginLoader.LoadPath = "./plugins/";
            _pluginLoader.LoadAll();

            LogManager.Instance.AppendLine("Spawning LoginServer proxy at port {0}.", _portCounter);
            _loginProxy = new RagProxy();
            _loginProxy.Prepare(_config.LoginEP, _config.ListenIP, _portCounter++);
            _loginProxy.Factory = new LoginProxyClientFactory();
        }

        public IPEndPoint SpawnCharServer(IPEndPoint ep, string name)
        {
            if (!_servers.ContainsKey(ep))
            {            
                int port = _portCounter++;

                LogManager.Instance.AppendLine("Spawning CharServer proxy for {0} at port {1}.", name, port);

                RagProxy rp = new RagProxy();
                rp.Factory = new CharProxyClientFactory();
                rp.Prepare(ep, _config.ListenIP, port);
                _servers.Add(ep, rp);
            }

            return (IPEndPoint)_servers[ep].Listener.Server.LocalEndPoint;
        }

        public IPEndPoint SpawnZoneServer(IPEndPoint ep)
        {
            if (!_servers.ContainsKey(ep))
            {
                int port = _portCounter++;

                LogManager.Instance.AppendLine("Spawning ZoneServer proxy at port {0}.", port);

                RagProxy rp = new RagProxy();
                rp.Factory = new ZoneProxyClientFactory();
                rp.Prepare(ep, _config.ListenIP, port);
                _servers.Add(ep, rp);
            }

            return (IPEndPoint)_servers[ep].Listener.Server.LocalEndPoint;
        }

        public void RegisterZoneHandler(IZoneHandler handler)
        {
            if (_zoneHandlers.Contains(handler))
                return;

            _zoneHandlers.Add(handler);
        }

        public void RegisterZoneCommand(string command, Action<ZoneProxyClient, string, string[]> action)
        {
            if (_zoneCommands.ContainsKey(command))
                return;

            _zoneCommands.Add(command, action);
        }

        public bool IsCommand(ZoneProxyClient client, string sender, string text)
        {
            bool isCmd = false;

            text = text.Trim();
            isCmd = text.StartsWith(CommandPrefix);

            if (isCmd)
            {
                text = text.Substring(1);

                ParseAndExecute(client, sender, text);
            }

            return isCmd;
        }

        private void ParseAndExecute(ZoneProxyClient client, string sender, string p)
        {
            var argsHolder = new List<string>();
            string tmp = "";
            bool inString = false;

            for (int i = 0; i < p.Length; i++)
            {
                char c = p[i];
                bool isLast = (i == p.Length - 1);

                if ((c == ' ' && !inString) || isLast)
                {
                    if (isLast)
                        tmp += c;

                    argsHolder.Add(tmp);
                    tmp = "";
                }
                else if (c == '"' && !inString)
                    inString = true;
                else if (c == '"' && inString)
                    inString = false;
                else if (c == '\\' && inString)
                {
                    switch (p[i + 1])
                    {
                        case '"':
                            tmp += '"';
                            i++;
                            break;
                        case '\\':
                            tmp += '\\';
                            i++;
                            break;
                        case 'n':
                            tmp += '\n';
                            i++;
                            break;
                        case 'r':
                            tmp += '\r';
                            i++;
                            break;
                        case 't':
                            tmp += '\t';
                            i++;
                            break;
                        case 'a':
                            tmp += '\a';
                            i++;
                            break;
                    }
                }
                else
                    tmp += c;
            }

            if (argsHolder.Count <= 0)
                return;

            string cmd = argsHolder[0];
            if (_zoneCommands.ContainsKey(cmd))
            {
                Action<ZoneProxyClient, string, string[]> act = (Action<ZoneProxyClient, string, string[]>)_zoneCommands[cmd];
                argsHolder.RemoveAt(0);

                try
                {
                    act(client, sender, argsHolder.ToArray());
                }
                catch (Exception ex)
                {
                    client.SendText("Error ocurred: {0}", ex.Message);
                }
            }
            else
            {
                client.SendText("Unknown command: {0}", cmd);
            }
        }

        public RagCookie GetCookie(int aid)
        {
            if (_cookies.ContainsKey(aid))
                return _cookies[aid];

            _cookies.Add(aid, new RagCookie());

            return _cookies[aid];
        }

        public void DeleteCookie(int aid)
        {
            if (_cookies.ContainsKey(aid))
                _cookies.Remove(aid);
        }
    }
}
