﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Plugin
{
    public class PluginLoader
    {
        private string _loadPath;
        public string LoadPath
        {
            get { return _loadPath; }
            set { _loadPath = value; }
        }

        private Dictionary<string, IPlugin> _plugins;
        private Dictionary<string, IPlugin> Plugins
        {
            get { return _plugins; }
        }

        public PluginLoader()
        {
            _plugins = new Dictionary<string, IPlugin>();
        }

        public void LoadAll()
        {
            string fp = Path.GetFullPath(_loadPath);
            string[] files = Directory.GetFiles(fp, "*.dll");

            foreach (string file in files)
            {
               // try
               // {
                    Assembly asm = null;

                    try
                    {
                        asm = Assembly.LoadFile(file);
                    }
                    catch
                    {
                        continue;
                    }

                    LoadPlugin(asm);
               // }
                //catch(Exception ex)
                //{
                //    LogManager.Instance.AppendLine("Plugin initialization failed: {0}", ex.Message);
                //}
            }
        }

        public void LoadPlugin(Assembly asm)
        {
            string clsname = Path.GetFileNameWithoutExtension(asm.Location) + ".RagnaLinkPlugin";
            Type t = asm.GetType(clsname);

            if (t == null)
                return;

            IPlugin plugin = (IPlugin)Activator.CreateInstance(t);

            if (plugin == null)
                return;

            plugin.Initialize();
            _plugins.Add(plugin.Name, plugin);
        }
    }
}
