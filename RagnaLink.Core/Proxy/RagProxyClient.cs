﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using RagnaLink.Core.Collections;
using RagnaLink.Core.IO;
using Timer = System.Timers.Timer;

namespace RagnaLink.Core.Proxy
{
    public class RagProxyClient : IServiceProvider
    {
        private class PacketData : IComparable<PacketData>
        {
            public byte[] Data { get; set; }
            public int Offset { get; set; }
            public int Size { get; set; }
            public DateTime Time { get; set; }

            public int CompareTo(PacketData other)
            {
                return this.Time.CompareTo(other.Time);
            }
        }

        private class PacketDispatcher
        {
            public PacketData Packet { get; set; }
            public Timer Timer { get; set; }

            public PacketDispatcher(RagProxyClient me, TcpClient client, PriorityQueue<PacketData> pq)
            {
                Timer = new Timer();
                Timer.AutoReset = false;
                Timer.Enabled = false;
                Timer.Elapsed += new ElapsedEventHandler((sender, e) =>
                {
                    me.StartAsyncWriteEx(client, pq, Packet, this);
                    Timer.Enabled = false;
                    Packet = null;
                });
            }
        }

        private QueueStream _clientInBuffer;  // Client -> Proxy
        private QueueStream _serverInBuffer;  // Server -> Proxy
        private PriorityQueue<PacketData> _clientOutQueue; // Proxy -> Client
        private PriorityQueue<PacketData> _serverOutQueue; // Proxy -> Server
        private PacketDispatcher _serverPacketCron;
        private PacketDispatcher _clientPacketCron;

        private Hashtable _services;

        private TcpClient _server;
        public TcpClient Server
        {
            get { return _server; }
        }

        private TcpClient _client;
        public TcpClient Client
        {
            get { return _client; }
        }

        private bool _disconnected;
        public bool Disconnected
        {
            get { return _disconnected; }
        }

        private int _connectionID;
        public int ConnectionID
        {
            get { return _connectionID; }
            set { _connectionID = value; }
        }

        protected bool _ignoreFirst4Bytes;

        public RagProxyClient()
        {
            _services = new Hashtable();
        }

        protected virtual void ParseCSPacket(ushort id, byte[] data, DateTime time)
        {
            SendDataToServer(data, time);
        }

        protected virtual void ParseSCPacket(ushort id, byte[] data, DateTime time)
        {
            SendDataToClient(data, time);
        }

        public void SetConnection(TcpClient server, TcpClient client)
        {
            _server = server;
            _client = client;

            _clientInBuffer = new QueueStream();
            _clientOutQueue = PriorityQueue<PacketData>.Synchronized(new PriorityQueue<PacketData>());
            _serverInBuffer = new QueueStream();
            _serverOutQueue = PriorityQueue<PacketData>.Synchronized(new PriorityQueue<PacketData>());

            _clientPacketCron = new PacketDispatcher(this, _client, _clientOutQueue);
            _serverPacketCron = new PacketDispatcher(this, _server, _serverOutQueue);
        }
        
        public virtual void StartRouting()
        {
            StartRouting(_clientInBuffer, _client, ParseCSPacket);
            StartRouting(_serverInBuffer, _server, ParseSCPacket);
        }

        public void SendDataToClient(byte[] data, int offset, int length, DateTime time)
        {
            bool start = _clientOutQueue.Count == 0;

            PacketData pd = new PacketData();
            pd.Data = data;
            pd.Offset = offset;
            pd.Size = length;
            pd.Time = time;

            lock (_clientPacketCron)
            {
                if (_clientPacketCron.Timer.Enabled && _clientPacketCron.Packet.Time < time)
                {
                    start = false;
                }
            }

            _clientOutQueue.Enqueue(pd);

            if (start)
                StartAsyncWrite(_client, _clientOutQueue, _clientPacketCron);
        }

        public void SendDataToClient(byte[] data, DateTime time)
        {
            SendDataToClient(data, 0, data.Length, time);
        }

        public void SendDataToClient(MemoryStream ms, DateTime time)
        {
            SendDataToClient(ms.GetBuffer(), 0, (int)ms.Length, time);
        }

        public void SendDataToServer(byte[] data, int offset, int length, DateTime time)
        {
            bool start = _serverOutQueue.Count == 0;

            PacketData pd = new PacketData();
            pd.Data = data;
            pd.Offset = offset;
            pd.Size = length;
            pd.Time = time;

            lock (_serverPacketCron)
            {
                if (_serverPacketCron.Timer.Enabled && _serverPacketCron.Packet.Time < time)
                {
                    start = false;
                }
            }

            _serverOutQueue.Enqueue(pd);

            if (start)
                StartAsyncWrite(_server, _serverOutQueue, _serverPacketCron);
        }

        public void SendDataToServer(byte[] data, DateTime time)
        {
            SendDataToServer(data, 0, data.Length, time);
        }

        public void SendDataToServer(MemoryStream ms, DateTime time)
        {
            SendDataToServer(ms.GetBuffer(), 0, (int)ms.Length, time);
        }

        public void Disconnect()
        {
            if (_disconnected)
                return;

            _disconnected = true;

            try
            {
                if (_client != null && _client.Connected)
                    _client.Client.Disconnect(true);
            }
            catch
            {

            }

            try
            {
                if (_server != null && _server.Connected)
                    _server.Client.Disconnect(true);
            }
            catch
            {

            }
        }

        private void ParsePackets(QueueStream qs, Action<ushort, byte[], DateTime> parser)
        {
            while (qs.Rest >= 2)
            {
                ushort id = BitConverter.ToUInt16(qs.Peek(0, 2), 0);
                PacketInfo pi = PacketLen.GetPacket(id);
                int size;
                byte[] data;

                if (_ignoreFirst4Bytes && qs.Rest >= 4)
                {
                    byte[] tmp = new byte[4];

                    qs.Read(tmp, 0, 4);
                    _connectionID = BitConverter.ToInt32(tmp, 0);
                    SendDataToClient(tmp, DateTime.Now);

                    _ignoreFirst4Bytes = false;

                    continue;
                }

                if (pi == null)
                {
                    LogManager.Instance.AppendLine("Client disconnected by invalid packet 0x{0:x}.", id);
                    Disconnect();
                    return;
                }

                if (pi.Size == -1)
                {
                    if (qs.Rest >= 4)
                    {
                        size = (int)BitConverter.ToUInt16(qs.Peek(2, 2), 0);
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    size = pi.Size;
                }

                if (qs.Rest < size)
                {
                    return;
                }

                data = new byte[size];
                qs.Read(data, 0, size);

                parser(id, data, DateTime.Now);
            }

            qs.Flush();
        }

        private void StartRouting(QueueStream qs, TcpClient client, Action<ushort, byte[], DateTime> parser)
        {
            byte[] buffer = new byte[4096];

            if (client == null || !client.Connected || _disconnected)
            {
                Disconnect();
                return;
            }

            try
            {
                client.Client.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback((ar) =>
                {
                    byte[] bbuffer = (byte[])ar.AsyncState;
                    int read = 0;

                    try
                    {
                        read = client.Client.EndReceive(ar);
                    }
                    catch
                    {
                        Disconnect();
                        return;
                    }

                    if (read == 0)
                    {
                        Disconnect();
                        return;
                    }

                    qs.Write(buffer, 0, read);

                    ParsePackets(qs, parser);
                    StartRouting(qs, client, parser);
                }), buffer);
            }
            catch
            {
                Disconnect();
                return;
            }
        }

        private void StartAsyncWrite(TcpClient client, PriorityQueue<PacketData> pq, PacketDispatcher cron)
        {
            PacketData pd = null;

            if (client == null || !client.Connected || _disconnected)
            {
                Disconnect();
                return;
            }

            lock (pq)
            {
                if (pq.Count == 0)
                    return;

                pd = pq.Dequeue();

                if (pd.Time > DateTime.Now)
                {
                    cron.Packet = pd;
                    cron.Timer.Interval = (pd.Time - DateTime.Now).TotalMilliseconds;
                    cron.Timer.Enabled = true;
                    return;
                }
            }

            if (pd == null)
                return;

            StartAsyncWriteEx(client, pq, pd, cron);
        }

        private void StartAsyncWriteEx(TcpClient client, PriorityQueue<PacketData> pq, PacketData pd, PacketDispatcher cron)
        {
            try
            {
                client.Client.BeginSend(pd.Data, pd.Offset, pd.Size, SocketFlags.None, new AsyncCallback((ar) =>
                {
                    PacketData pdd = (PacketData)ar.AsyncState;
                    int sent = 0;

                    try
                    {
                        sent = client.Client.EndSend(ar);
                    }
                    catch
                    {
                        Disconnect();
                        return;
                    }

                    if (sent < pdd.Size)
                    {
                        pdd.Offset += sent;
                        pdd.Size -= sent;
                        StartAsyncWriteEx(client, pq, pdd, cron);
                    }
                    else
                    {
                        StartAsyncWrite(client, pq, cron);
                    }
                }), pd);
            }
            catch
            {
                Disconnect();
                return;
            }
        }

        public object GetService(Type serviceType)
        {
            if (_services.ContainsKey(serviceType))
                return _services[serviceType];

            return null;
        }

        public bool RegisterService(Type serviceType, object instance)
        {
            if (_services.ContainsKey(serviceType))
                return false;

            _services.Add(serviceType, instance);

            return true;
        }

        public bool RegisterService(object instance)
        {
            return RegisterService(instance.GetType(), instance);
        }

        public T GetService<T>()
        {
            return (T)GetService(typeof(T));
        }
    }
}
