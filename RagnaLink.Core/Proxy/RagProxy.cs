﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using RagnaLink.Core.IO;

namespace RagnaLink.Core.Proxy
{
    public class RagProxy
    {
        private TcpListener _listener;
        public TcpListener Listener
        {
            get { return _listener; }
        }

        private RagProxyClientFactory _factory;
        public RagProxyClientFactory Factory
        {
            get { return _factory; }
            set { _factory = value; }
        }

        private IPEndPoint _endPoint;
        public IPEndPoint EndPoint
        {
            get { return _endPoint; }
            set { _endPoint = value; }
        }

        public RagProxy()
        {

        }

        public void Prepare(IPEndPoint ep, IPAddress localIP,  int localPort)
        {
            _listener = new TcpListener(localIP, localPort);
            _endPoint = ep;

            _listener.Start();
            ReAccept();
        }

        private void ReAccept()
        {
            _listener.AcceptTcpClientAsync().ContinueWith(new Action<Task<TcpClient>>((t) =>
            {
                RagProxyClient client = _factory.Create(this);

                TcpClient server = new TcpClient();

                try
                {
                    server.Connect(_endPoint);
                }
                catch
                {
                    t.Result.Close();
                }

                client.SetConnection(server, t.Result);
                client.StartRouting();

                ReAccept();
            }));
        }
    }
}
