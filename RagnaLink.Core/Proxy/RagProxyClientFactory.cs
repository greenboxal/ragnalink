﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Proxy
{
    public abstract class RagProxyClientFactory
    {
        public abstract RagProxyClient Create(RagProxy owner);
    }
}
