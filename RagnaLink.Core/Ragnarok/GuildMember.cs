﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RagnaLink.Core.Ragnarok
{
    public class GuildMember
    {
        public int ID { get; private set; }
        public string Position { get; internal set; }

        public GuildMember(int id)
        {
            ID = id;
        }
    }
}
