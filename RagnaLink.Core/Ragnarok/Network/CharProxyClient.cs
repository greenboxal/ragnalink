﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using RagnaLink.Core.Proxy;

namespace RagnaLink.Core.Ragnarok.Network
{
    public class CharProxyClient : RagProxyClient
    {
        private RagProxy _owner;
        public RagProxy Owner
        {
            get { return _owner; }
        }

        public CharProxyClient(RagProxy owner)
        {
            _owner = owner;
        }

        public override void StartRouting()
        {
            LogManager.Instance.AppendLine("Client connected to CharServer {0}.", ((IPEndPoint)_owner.Listener.Server.LocalEndPoint).Port - LinkManager.Instance.Config.ListenPortBase);

            base.StartRouting();
        }

        protected override void ParseCSPacket(ushort id, byte[] data, DateTime time)
        {
            if (id == 0x65)
            {
                _ignoreFirst4Bytes = true;
            }

            base.ParseCSPacket(id, data, time);
        }

        protected override void ParseSCPacket(ushort id, byte[] data, DateTime time)
        {
            bool send = true;

            if (id == 0x71)
            {
                BinaryReader br = new BinaryReader(new MemoryStream(data));
                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);

                br.BaseStream.Position = 2;

                int cid = br.ReadInt32();
                string mapname = br.ReadCString(16);
                IPAddress ip = new IPAddress(br.ReadBytes(4));
                ushort port = br.ReadUInt16();

                RagCookie rc = LinkManager.Instance.GetCookie(ConnectionID);
                rc.StartMap = mapname;

                IPEndPoint proxyEP = LinkManager.Instance.SpawnZoneServer(new IPEndPoint(ip, port));

                bw.Write((ushort)0x71);
                bw.Write((int)cid);
                bw.WriteCString(mapname, 16);
                bw.Write(proxyEP.Address.GetAddressBytes(), 0, 4);
                bw.Write((ushort)proxyEP.Port);

                bw.Flush();
                send = false;
                SendDataToClient(ms, time);
            }

            if (send)
                SendDataToClient(data, time);
        }
    }

    public class CharProxyClientFactory : RagProxyClientFactory
    {
        public override RagProxyClient Create(RagProxy owner)
        {
            return new CharProxyClient(owner);
        }
    }
}
