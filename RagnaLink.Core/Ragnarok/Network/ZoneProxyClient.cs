﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using RagnaLink.Core.Proxy;

namespace RagnaLink.Core.Ragnarok.Network
{
    public class ZoneProxyClient : RagProxyClient
    {

        private RagProxy _owner;
        public RagProxy Owner
        {
            get { return _owner; }
        }

        private RagCookie _cookie;
        public RagCookie Cookie
        {
            get { return _cookie; }
        }

        public ZoneProxyClient(RagProxy owner)
        {
            _owner = owner;
        }

        public override void StartRouting()
        {
            LogManager.Instance.AppendLine("Client connected to ZoneServer {0}.", ((IPEndPoint)_owner.Listener.Server.LocalEndPoint).Port - LinkManager.Instance.Config.ListenPortBase);

            for (int i = 0; i < LinkManager.Instance.ZoneHandlers.Count; i++)
                LinkManager.Instance.ZoneHandlers[i].OnClientStart(this);

            base.StartRouting();
        }

        protected override void ParseCSPacket(ushort id, byte[] data, DateTime time)
        {
            bool send = true;

            if (id == 0x7d)
            {
                SendText("You are connected through RagnaLink. Have fun!");
            }
            else if (id == 0x8c || (id == 0xf3 && (PacketLen.GetPacket(id).Size == -1))) // iRO and kRO chat packet
            {
                BinaryReader br = new BinaryReader(new MemoryStream(data));
                MemoryStream ms = new MemoryStream();

                br.BaseStream.Position = 4;

                string[] parts = br.ReadCString(data.Length - 4).Split(new string[] { " : " }, 2, StringSplitOptions.None);

                if (parts.Length != 2)
                {
                    LogManager.Instance.AppendLine("Malformed GlobalMessage packet.", id);
                }

                send = !LinkManager.Instance.IsCommand(this, parts[0], parts[1]);

                for (int i = 0; i < LinkManager.Instance.ZoneHandlers.Count && send; i++)
                    send = !LinkManager.Instance.ZoneHandlers[i].OnChatSent(this, parts[0], parts[1]);
            }

            if (send)
            {
                for (int i = 0; i < LinkManager.Instance.ZoneHandlers.Count && send; i++)
                    send = !LinkManager.Instance.ZoneHandlers[i].OnPacketSent(this, id, data, time);

                if (send)
                    SendDataToServer(data, time);
            }
        }

        protected override void ParseSCPacket(ushort id, byte[] data, DateTime time)
        {
            bool send = true;
            
            if (id == 0x283) // ZC_ACCOUNT_ID
            {
                ConnectionID = BitConverter.ToInt32(data, 2);
                _cookie = LinkManager.Instance.GetCookie(ConnectionID);
                _cookie.Speed = 150; // FIX ME WHILE I'M NEW =P
            }
            else if (id == 0xb0) // ZC_PAR_CHANGE
            {
                ushort type = BitConverter.ToUInt16(data, 2);

                if (type == 0)
                {
                    _cookie.Speed = BitConverter.ToUInt32(data, 4);
                }
            }
            else if (id == 0x73 || id == 0x2eb) // ZC_ACCEPT_ENTER || ZC_ACCEPT_ENTER2
            {
                int x, y, dir;

                Utils.DecodePosition(data, 6, out x, out y, out dir);
                ChangePosition(x, y, x, y, true);
            }
            else if (id == 0x86) // ZC_NOTIFY_PLAYERMOVE
            {
                if (BitConverter.ToInt32(data, 2) == ConnectionID)
                {
                    int x, y, tx, ty, sx, sy;

                    Utils.DecodePosition2(data, 6, out x, out y, out tx, out ty, out sx, out sy);
                    ChangePosition(x, y, tx, ty, false);

                    //SendText("Moving from ({0}, {1}) to ({2}, {3})...", x, y, tx, ty);
                }
            }
            else if (id == 0x87) // ZC_NOTIFY_PLAYERMOVE
            {
                int x, y, tx, ty, sx, sy;

                Utils.DecodePosition2(data, 6, out x, out y, out tx, out ty, out sx, out sy);
                ChangePosition(x, y, tx, ty, false);

               // SendText("Moving from ({0}, {1}) to ({2}, {3})...", x, y, tx, ty);
            }
            else if (id == 0x88 || id == 0x1ff) // ZC_STOPMOVE || ZC_HIGHJUMP
            {
                if (BitConverter.ToInt32(data, 2) == ConnectionID)
                {
                    int x = BitConverter.ToInt16(data, 6);
                    int y = BitConverter.ToInt16(data, 8);

                    ChangePosition(x, y, x, y, true);
                    //SendText("Moving to ({0}, {1})...", x, y);
                }
            }
            else if (id == 0x91) // ZC_NPCACK_MAPMOVE
            {
                BinaryReader br = new BinaryReader(new MemoryStream(data));

                br.BaseStream.Position = 2;

                _cookie.StartMap = br.ReadCString(16);

                int x = br.ReadUInt16();
                int y = br.ReadUInt16();

                ChangePosition(x, y, x, y, true);

                //SendText("Moving to ({0}, {1})...", x, y);
            }
            else if (id == 0x92) // ZC_NPCACK_SERVERMOVE
            {
                BinaryReader br = new BinaryReader(new MemoryStream(data));
                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);

                br.BaseStream.Position = 2;

                string mapname = br.ReadCString(16);
                ushort x = br.ReadUInt16();
                ushort y = br.ReadUInt16();
                IPAddress ip = new IPAddress(br.ReadBytes(4));
                ushort port = br.ReadUInt16();

                _cookie.StartMap = mapname;
                ChangePosition(x, y, x, y, true);

                IPEndPoint proxyEP = LinkManager.Instance.SpawnZoneServer(new IPEndPoint(ip, port));

                bw.Write((ushort)0x92);
                bw.WriteCString(mapname, 16);
                bw.Write((ushort)x);
                bw.Write((ushort)y);
                bw.Write(proxyEP.Address.GetAddressBytes(), 0, 4);
                bw.Write((ushort)proxyEP.Port);

                bw.Flush();
                send = false;
                SendDataToClient(ms, time);
            }
            else if (id == 0x8d || id == 0x8e) // ZC_NOTIFY_CHAT || ZC_NOTIFY_PLAYERCHAT
            {
                BinaryReader br = new BinaryReader(new MemoryStream(data));
                MemoryStream ms = new MemoryStream();

                br.BaseStream.Position = 4;

                int senderID = (id == 0x8d) ? br.ReadInt32() : ConnectionID;
                string[] parts = br.ReadCString(data.Length - 4).Split(new string[] { " : " }, 2, StringSplitOptions.None);
                string senderName, message;

                if (parts.Length == 0)
                {
                    LogManager.Instance.AppendLine("Malformed GlobalMessage packet.", id);
                    senderName = "";
                    message = "";
                }
                else if (parts.Length < 2)
                {
                    senderName = "";
                    message = parts[0];
                }
                else
                {
                    senderName = parts[0];
                    message = String.Join(" : ", parts.Skip(1));
                }

                for (int i = 0; i < LinkManager.Instance.ZoneHandlers.Count && send; i++)
                    send = !LinkManager.Instance.ZoneHandlers[i].OnChatReceived(this, senderID, senderName, message);
            }

            if (send)
            {
                for (int i = 0; i < LinkManager.Instance.ZoneHandlers.Count && send; i++)
                    send = !LinkManager.Instance.ZoneHandlers[i].OnPacketReceived(this, id, data, time);

                if (send)
                    SendDataToClient(data, time);
            }
        }

        private void ChangePosition(int x, int y, int tx, int ty, bool teleport)
        {
            bool handled = true;

            _cookie.X = x;
            _cookie.Y = y;
            _cookie.TX = tx;
            _cookie.TY = ty;

            for (int i = 0; i < LinkManager.Instance.ZoneHandlers.Count && handled; i++)
                handled = !LinkManager.Instance.ZoneHandlers[i].OnClientWalk(this, LinkManager.Instance.GetCookie(ConnectionID).StartMap, x, y, tx, ty, teleport);
        }

        public void SendText(string text, params object[] args)
        {
            SendText(0x00FFFF00, text, args);
        }

        public void SendText(DateTime time, string text, params object[] args)
        {
            SendText(0x00FFFF00, time, text, args);
        }

        public void SendText(uint argb, string text, params object[] args)
        {
            SendText(argb, DateTime.Now, text, args);
        }

        public void SendText(uint argb, DateTime time, string text, params object[] args)
        {
            string msg = string.Format(text, args);
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);

            bw.Write((ushort)0x2c1);
            bw.Write((ushort)(msg.Length + 13));
            bw.Write((uint)0); // AID
            bw.Write((uint)((argb & 0x000000FF) << 16 | (argb & 0x0000FF00) | (argb & 0x00FF0000) >> 16) | (argb & 0xFF000000)); // Color ABGR
            bw.WriteCString(msg, msg.Length + 1);

            bw.Flush();
            SendDataToClient(ms, time);
        }
    }

    public class ZoneProxyClientFactory : RagProxyClientFactory
    {
        public override RagProxyClient Create(RagProxy owner)
        {
            return new ZoneProxyClient(owner);
        }
    }
}
