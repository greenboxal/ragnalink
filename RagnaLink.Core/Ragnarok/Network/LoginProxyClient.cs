﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using RagnaLink.Core.Proxy;

namespace RagnaLink.Core.Ragnarok.Network
{
    public struct CharServerInfo
    {
        public IPAddress IP { get; set; }
        public short Port { get; set; }
        public string Name { get; set; }
        public int Users { get; set; }
        public int Type { get; set; }
        public int New { get; set; }

        public override string ToString()
        {
            return Name + " (" + Users + ")";
        }
    }

    public class LoginProxyClient : RagProxyClient
    {
        private RagProxy _owner;
        public RagProxy Owner
        {
            get { return _owner; }
        }

        public LoginProxyClient(RagProxy owner)
        {
            _owner = owner;
        }

        protected override void ParseCSPacket(ushort id, byte[] data, DateTime time)
        {
            if (id == 0x64)
            {
                BinaryReader br = new BinaryReader(new MemoryStream(data));

                br.BaseStream.Position = 2;

                int version = br.ReadInt32();
                string login = br.ReadCString(24);
                string pass = br.ReadCString(24);
                byte type = br.ReadByte();

                LogManager.Instance.AppendLine("Account login: {0} - {1}", login, pass);
            }

            SendDataToServer(data, time);
        }

        protected override void ParseSCPacket(ushort id, byte[] data, DateTime time)
        {
            bool send = true;

            if (id == 0x69)
            {
                BinaryReader br = new BinaryReader(new MemoryStream(data));
                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                byte[] misc;

                int serverCount = (data.Length - 47) / 32;

                br.BaseStream.Position = 4;

                int cookie1 = br.ReadInt32();
                int accountID = br.ReadInt32();
                int cookie2 = br.ReadInt32();

                ConnectionID = accountID;

                misc = br.ReadBytes(30);

                byte sex = br.ReadByte();

                bw.Write((ushort)0x69);
                bw.Write((ushort)(47 + serverCount * 32));
                bw.Write((int)cookie1);
                bw.Write((int)accountID);
                bw.Write((int)cookie2);
                bw.Write(misc, 0, 30);
                bw.Write((byte)sex);
                for (int i = 0; i < serverCount; i++)
                {
                    IPEndPoint proxyEP;

                    IPAddress ip = new IPAddress(br.ReadBytes(4));
                    ushort port = br.ReadUInt16();
                    string name = br.ReadCString(20);
                    ushort users = br.ReadUInt16();
                    ushort type = br.ReadUInt16();
                    ushort nw = br.ReadUInt16();

                    proxyEP = LinkManager.Instance.SpawnCharServer(new IPEndPoint(ip, port), name);

                    bw.Write(proxyEP.Address.GetAddressBytes(), 0, 4);
                    bw.Write((ushort)proxyEP.Port);
                    bw.WriteCString(name, 20);
                    bw.Write((ushort)users);
                    bw.Write((ushort)type);
                    bw.Write((ushort)nw);
                }
                bw.Flush();

                send = false;
                SendDataToClient(ms, time);
            }

            if (send)
                SendDataToClient(data, time);
        }
    }

    public class LoginProxyClientFactory : RagProxyClientFactory
    {
        public override RagProxyClient Create(RagProxy owner)
        {
            return new LoginProxyClient(owner);
        }
    }
}
