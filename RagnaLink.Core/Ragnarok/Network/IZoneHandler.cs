﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Ragnarok.Network
{
    public interface IZoneHandler
    {
        void OnClientStart(ZoneProxyClient client);
        bool OnChatReceived(ZoneProxyClient client, int senderID, string senderName, string text);
        bool OnChatSent(ZoneProxyClient client, string sender, string text);
        bool OnPacketSent(ZoneProxyClient client, ushort id, byte[] data, DateTime time);
        bool OnPacketReceived(ZoneProxyClient client, ushort id, byte[] data, DateTime time);
        bool OnClientWalk(ZoneProxyClient client, string mapname, int x, int y, int tx, int ty, bool teleport);
    }
}
