﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Ragnarok.Network
{
    public class RagCookie
    {
        public string StartMap { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int TX { get; set; }
        public int TY { get; set; }
        public uint Speed { get; set; }
    }
}
