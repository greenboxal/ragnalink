﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Ragnarok
{
    public struct Skill
    {
        public int ID;
        public int Level;
        public int MaxLevel;
    }
}
