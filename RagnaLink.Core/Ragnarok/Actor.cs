﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Ragnarok
{
    public abstract class Actor
    {
        public int ID { get; private set; }
        public string Name { get; internal set; }
        public short X { get; internal set; }
        public short Y { get; internal set; }
       
        public Actor(int id)
        {
            ID = id;
        }
    }
}
