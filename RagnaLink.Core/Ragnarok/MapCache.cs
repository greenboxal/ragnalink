﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zlib;
using RagnaLink.Core.Algorithms;

namespace RagnaLink.Core.Ragnarok
{
    public class MapCache
    {
        private const uint HeaderMagic = 0xdeadbeef;

        private Hashtable _maps;
        public Hashtable Maps
        {
            get { return _maps; }
        }

        public MapCache()
        {
            _maps = new Hashtable();
        }

        public MapData GetMap(string name)
        {
            if (name.EndsWith(".gat"))
                name = name.Substring(0, name.Length - 4);

            if (_maps.ContainsKey(name))
                return (MapData)_maps[name];

            return null;
        }

        public bool Load(Stream s)
        {
            BinaryReader br = new BinaryReader(s, Encoding.UTF8, true);
            List<MapData> mapOrder = new List<MapData>();

            if (br.ReadUInt32() != HeaderMagic)
                return false;

            int mapCount = br.ReadInt32();

            for (int i = 0; i < mapCount; i++)
            {
                MapData map = new MapData();

                map.Name = br.ReadString();
                map.Width = br.ReadInt32();
                map.Height = br.ReadInt32();

                _maps[map.Name] = map;
                mapOrder.Add(map);
            }
            
            for (int i = 0; i < mapCount; i++)
            {
                int size = br.ReadInt32();
                byte[] data = br.ReadBytes(size);

                data = ZlibStream.UncompressBuffer(data);
                mapOrder[i].Cells = new CellTypeWrapper[mapOrder[i].Width * mapOrder[i].Height];

                for (int j = 0; j < mapOrder[i].Cells.Length; j++)
                {
                    mapOrder[i].Cells[j].Type = (CellType)data[j];
                }
            }
            
            br.Close();

            return true;
        }

        public void Save(Stream s)
        {
            BinaryWriter bw = new BinaryWriter(s, Encoding.UTF8, true);

            bw.Write(HeaderMagic);
            bw.Write(_maps.Count);

            foreach (DictionaryEntry entry in _maps)
            {
                MapData map = (MapData)entry.Value;

                bw.Write(map.Name);
                bw.Write(map.Width);
                bw.Write(map.Height);
            }

            foreach (DictionaryEntry entry in _maps)
            {
                MapData map = (MapData)entry.Value;
                byte[] data = new byte[map.Width * map.Height];

                for (int i = 0; i < map.Cells.Length; i++)
                {
                    data[i] = (byte)map.Cells[i].Type;
                }

                data = ZlibStream.CompressBuffer(data);

                bw.Write(data.Length);
                bw.Write(data);
            }

            bw.Flush();
            bw.Close();
        }
    }
    
    public enum CellType : byte
    {
        NonWalkable,
        Walkable,
        Water,
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CellTypeWrapper
    {
        public CellType Type { get; set; }
    }

    public class MapData : IMap
    {
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public CellTypeWrapper[] Cells { get; set; }

        public uint GetCellWeight2D(Point2D point)
        {
            int index = point.Y * Width + point.X;

            if (index < Cells.Length)
                return Cells[index].Type == CellType.NonWalkable ? 0 : (uint)1;

            return 0;
        }

        public uint GetCellWeight3D(Point3D point)
        {
            throw new NotSupportedException();
        }
    }
}
