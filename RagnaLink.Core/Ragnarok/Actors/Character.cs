﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Ragnarok.Actors
{
    /* public enum CharacterSex : byte
    {
        Male = false,
        Female = true,
    } */
    public class Character : WalkableActor
    {
        public byte HeadDirection { get; internal set; }
        public short Level { get; internal set; }
        public short Class { get; internal set; }
        
        public short BodyState { get; internal set; }
        public short HealthState { get; internal set; }
        public short EffectState { get; internal set; }

        public byte HairStyle { get; internal set; }
        public short HairColor { get; internal set; }
        public short ClothesColor { get; internal set; }

        public short WeaponID { get; internal set; }
        public short ShieldID { get; internal set; }

        public short HeadgearTop { get; internal set; }
        public short HeadgearMiddle { get; internal set; }
        public short HeadgearBottom { get; internal set; }
        public short Robe { get; internal set; }

        public Guild Guild { get; internal set; }
        public Party Party { get; internal set; }

        public bool Sex { get; internal set; }

        public bool Muted { get; internal set; }

        public string ClassName { get; set; /* { return ClassNames[Class]; } */ }

        public Character(int id)
            : base(id)
        {

        }
    }
}
