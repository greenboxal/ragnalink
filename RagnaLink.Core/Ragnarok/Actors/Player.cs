﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RagnaLink.Core.Ragnarok.Network;

namespace RagnaLink.Core.Ragnarok.Actors
{
    public class Player : Character
    {
        public long Experience { get; internal set; }
        public long NextLevelExperience { get; internal set; }

        public ZoneProxyClient Client { get; private set; }
        public List<Skill> SkillTree { get; private set; }

        public Player(ZoneProxyClient client)
            : base(client.ConnectionID)
        {
            Client = client;

            SkillTree = new List<Skill>();
        }
    }
}
