﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Ragnarok.Actors
{
    public abstract class WalkableActor : Actor
    {
        public short WalkDestinationX { get; internal set; }
        public short WalkDestinationY { get; internal set; }

        public byte Direction { get; internal set; }
        public short Speed { get; internal set; }

        public WalkableActor(int id)
            : base(id)
        {

        }
    }
}
