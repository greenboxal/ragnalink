﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Ragnarok
{
    public class Guild
    {
        public int ID { get; private set; }

        public bool Incomplete { get; internal set; }

        public short Level { get; internal set; }
        
        public short ConnectedMembers { get; internal set; }
        public short MaxMembers { get; internal set; }

        public long Experience { get; internal set; }
        public long NextLevelExperience { get; internal set; }

        public string Name { get; internal set; }
        public string Master { get; internal set; }

        public Emblem Emblem { get; internal set; }

        public List<GuildMember> Members { get; internal set; }

        public string[] Messages { get; internal set; }

        public List<Guild> Allies { get; internal set; }
        public List<Guild> Rivals { get; internal set; }

        public List<Skill> SkillTree { get; private set; }

        public Guild(int id)
        {
            ID = id;
            SkillTree = new List<Skill>();
        }
    }
}
