﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Ragnarok
{
    public class Party
    {
        public int ID { get; private set; }

        public short ConnectedMembers { get; internal set; }
        public short MaxMembers { get; internal set; }

        public List<int> Members { get; internal set; }
    }
}
