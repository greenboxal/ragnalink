﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Algorithms
{
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct Point2D
    {
        public int X;
        public int Y;

        public Point2D(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
