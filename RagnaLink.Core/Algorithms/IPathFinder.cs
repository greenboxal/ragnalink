﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Algorithms
{
    public interface IPathFinder
    {
        IMap Map { get; }
        Stack<Point2D> FindPath2D(Point2D start, Point2D end);
        Stack<Point3D> FindPath3D(Point3D start, Point3D end);
    }
}
