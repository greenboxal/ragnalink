﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RagnaLink.Core.Collections;

namespace RagnaLink.Core.Algorithms
{
    public enum PathFinderHeuristic
    {
        ManhattanDistance,
        EuclideanDistance,
    }

    public class PathFinderNode : IComparable<PathFinderNode>
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int D { get; set; }
        public PathFinderNode P { get; set; }
        
        public int CompareTo(PathFinderNode other)
        {
            return other.D.CompareTo(this.D);
        }

        public PathFinderNode(int x, int y, int d)
        {
            X = x;
            Y = y;
            D = d;
            P = null;
        }

        public PathFinderNode(int x, int y)
        {
            X = x;
            Y = y;
            D = 0;
            P = null;
        }

    }

    public class PathFinder : IPathFinder
    {
        public IMap Map { get; private set; }
        public PathFinderHeuristic Heuristic { get; set; }

        public const int AreaSize = 14;

        public PathFinder(IMap map)
        {
            Map = map;
        }

        private bool canWalk(int x, int y, int tx, int ty)
        {
            if ((tx == x - 1 && ty == y - 1) || // up-left corner
                (tx == x - 1 && ty == y + 1) || // down-left corner
                (tx == x + 1 && ty == y + 1) || // down-right corner
                (tx == x + 1 && ty == y - 1))   // up-right corner
                return Map.GetCellWeight2D(new Point2D(tx, ty)) != 0 && Map.GetCellWeight2D(new Point2D(tx, y)) != 0 && Map.GetCellWeight2D(new Point2D(x, ty)) != 0;

            return Map.GetCellWeight2D(new Point2D(tx, ty)) != 0;
        }

        private int manhattanDistance(PathFinderNode src, PathFinderNode target)
        {
            return (Math.Abs(src.X - target.X) + Math.Abs(src.Y - target.Y)) * 10;
        }

        private int euclideanDistance(PathFinderNode src, PathFinderNode target)
        {
            int a = src.X - target.X;
            int b = src.Y - target.Y;
            return a * a + b * b;
        }

        public Stack<Point2D> FindPath2D(Point2D source, Point2D dest)
        {
            PriorityQueue<PathFinderNode> openList = new PriorityQueue<PathFinderNode>();
            List<Point2D> closedList = new List<Point2D>();
            Dictionary<Point2D, PathFinderNode> nodes = new Dictionary<Point2D, PathFinderNode>();
            PathFinderNode sourceNode = new PathFinderNode(source.X, source.Y);
            PathFinderNode destNode = new PathFinderNode(dest.X, dest.Y);
            bool found = false;

            nodes.Add(dest, destNode);
            nodes.Add(source, sourceNode);

            openList.Enqueue(sourceNode);

            while (openList.Count > 0 && !found)
            {
                PathFinderNode currNode = openList.Dequeue();
                Point2D currNodePoint = new Point2D(currNode.X, currNode.Y);
                closedList.Add(currNodePoint);

                if (nodes.ContainsKey(currNodePoint))
                    nodes.Remove(currNodePoint);

                if (currNode.X == destNode.X && currNode.Y == destNode.Y)
                {
                    found = true;
                    break;
                }

                int[,] adjacents = new int[8, 2] {
                                     { currNode.X + 0, currNode.Y - 1 },
                                     { currNode.X - 1, currNode.Y - 1 },
                                     { currNode.X - 1, currNode.Y + 0 },
                                     { currNode.X - 1, currNode.Y + 1 },
                                     { currNode.X + 0, currNode.Y + 1 },
                                     { currNode.X + 1, currNode.Y + 1 },
                                     { currNode.X + 1, currNode.Y + 0 },
                                     { currNode.X + 1, currNode.Y - 0 },
                                   };

                for (int i = 0; i < 8; i++)
                {
                    Point2D point = new Point2D(adjacents[i, 0], adjacents[i, 1]);

                    if (!nodes.ContainsKey(point))
                        nodes.Add(point, new PathFinderNode(point.X, point.Y));

                    if (canWalk(currNode.X, currNode.Y, adjacents[i, 0], adjacents[i, 1]))
                    {
                        if (closedList.Contains(point))
                            continue;

                        if (!openList.Contains(nodes[point]))
                        {
                            PathFinderNode node = nodes[point];
                            node.X = point.X;
                            node.Y = point.Y;
                            node.P = currNode;
                            switch (Heuristic)
                            {
                                case PathFinderHeuristic.ManhattanDistance:
                                    node.D = manhattanDistance(node, destNode);
                                    break;
                                case PathFinderHeuristic.EuclideanDistance:
                                    node.D = euclideanDistance(node, destNode);
                                    break;
                            }
                            openList.Enqueue(node);
                        }
                    }
                }

            }

            if (!found)
                return null;

            Stack<Point2D> path = new Stack<Point2D>();
            {
                PathFinderNode currNode = destNode;
                Point2D lastPoint = new Point2D(-5000, -5000);
                do
                {
                    Point2D currPoint = new Point2D(currNode.X, currNode.Y);
                    if (Math.Abs(currPoint.X - lastPoint.X) >= AreaSize ||
                        Math.Abs(currPoint.Y - lastPoint.Y) >= AreaSize)
                    {
                        path.Push(currPoint);
                        lastPoint = currPoint;
                    }
                    currNode = currNode.P;
                } while (currNode.X != sourceNode.X || currNode.Y != sourceNode.Y);
            }


            return path;
        }

        public Stack<Point3D> FindPath3D(Point3D start, Point3D end)
        {
            throw new NotSupportedException();
        }
    }
}
