﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Algorithms
{
    public class AStartPathFinder : IPathFinder
    {
        private struct AStartNode
        {
            public Point2D Point;
            public int Parent;
            public uint G, F, H;

            public static AStartNode Create(Point2D point, int parent)
            {
                AStartNode node = new AStartNode();

                node.Point = point;
                node.Parent = parent;
                node.F = 0;
                node.G = 0;

                return node;
            }

            public static AStartNode Create(Point2D point, int parent, uint g, uint h)
            {
                AStartNode node = new AStartNode();

                node.Point = point;
                node.Parent = parent;
                node.G = g;
                node.H = h;
                node.F = g + h;

                return node;
            }
        }

        private struct QuickFindNode
        {
            public int Index;
            public uint Value;

            public static QuickFindNode Create(int index, uint value)
            {
                QuickFindNode node = new QuickFindNode();

                node.Index = index;
                node.Value = value;

                return node;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int QuickFind(List<QuickFindNode> nodes, uint value, int low, int high)
        {
            int x = (low + high) >> 1;

            if (nodes[x].Value == value)
                return x;
            else if (x != high - 1 && value < nodes[x].Value)
                return QuickFind(nodes, value, x, high);
            else if (x != low && value > nodes[x].Value)
                return QuickFind(nodes, value, low, x);
            else if (nodes[x].Value > value)
                return x + 1;
            else
                return x;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool CheckPosition(IMap map, List<AStartNode> nodes, int currentNode, int x, int y)
        {
            Point2D pos = new Point2D();

            pos.X = nodes[currentNode].Point.X + x;
            pos.Y = nodes[currentNode].Point.Y + y;

            if (map.GetCellWeight2D(pos) != 0)
            {
                if (!(nodes[currentNode].Parent != -1 &&
                    nodes[nodes[currentNode].Parent].Point.X == pos.X &&
                    nodes[nodes[currentNode].Parent].Point.Y == pos.Y))
                {
                    nodes.Add(AStartNode.Create(pos, -1));
                    return true;
                }
            }

            return false;
        }

        public IMap Map { get; private set; }
        public bool CheckDiagonals { get; set; }

        public AStartPathFinder(IMap map)
        {
            Map = map;
        }

        public Stack<Point2D> FindPath2D(Point2D start, Point2D end)
        {
            List<QuickFindNode> openList = new List<QuickFindNode>();
            List<AStartNode> fullList = new List<AStartNode>();
            int currentNode;

            fullList.Add(AStartNode.Create(start, -1));
            openList.Add(QuickFindNode.Create(0, fullList[0].F));

            if (Map.GetCellWeight2D(start) == 0 || Map.GetCellWeight2D(end) == 0)
                return null;

            uint[] lookup = new uint[Map.Width * Map.Height];
            for (int i = 0; i < lookup.Length; i++)
            {
                lookup[i] = 999999;
            }

            while (true)
            {
                if (openList.Count == 0)
                    return null;

                currentNode = openList.Last().Index;
                openList.RemoveAt(openList.Count - 1);

                int index = fullList[currentNode].Point.Y * Map.Width + fullList[currentNode].Point.X;
                if (fullList[currentNode].G > lookup[index])
                    continue;

                if (fullList[currentNode].Point.X == end.X && fullList[currentNode].Point.Y == end.Y)
                {
                    Stack<Point2D> path = new Stack<Point2D>();

                    do
                    {
                        path.Push(fullList[currentNode].Point);
                        currentNode = fullList[currentNode].Parent;
                    }
                    while (currentNode != -1);

                    return path;
                }

                int successors_start = fullList.Count;
                int successors_size = 0;

                if (CheckPosition(Map, fullList, currentNode, -1, 0))
                    successors_size++;

                if (CheckPosition(Map, fullList, currentNode, 0, -1))
                    successors_size++;

                if (CheckPosition(Map, fullList, currentNode, 1, 0))
                    successors_size++;

                if (CheckPosition(Map, fullList, currentNode, 0, 1))
                    successors_size++;

                if (CheckDiagonals)
                {
                    if (CheckPosition(Map, fullList, currentNode, -1, -1))
                        successors_size++;

                    if (CheckPosition(Map, fullList, currentNode, 1, 1))
                        successors_size++;

                    if (CheckPosition(Map, fullList, currentNode, -1, 1))
                        successors_size++;

                    if (CheckPosition(Map, fullList, currentNode, 1, -1))
                        successors_size++;
                }

                for (int i = 0; i < successors_size; i++)
                {
                    int idx = successors_start + i;
                    int nidx;

                    AStartNode node = fullList[idx];
                    QuickFindNode qnode = new QuickFindNode();

                    index = fullList[idx].Point.Y * Map.Width + fullList[idx].Point.X;

                    node.G = fullList[currentNode].G + Map.GetCellWeight2D(node.Point);
                    node.H = CalculateHeuristc(start, end, node.Point);
                    node.F = node.G + node.H;
                    node.Parent = currentNode;

                    if (node.G >= lookup[index])
                        continue;

                    fullList[idx] = node;

                    lookup[index] = node.G;

                    if (openList.Count > 0)
                        nidx = QuickFind(openList, fullList[idx].F, 0, openList.Count);
                    else
                        nidx = 0;

                    qnode.Index = idx;
                    qnode.Value = fullList[idx].F;

                    openList.Insert(nidx, qnode);
                }
            }
        }

        public Stack<Point3D> FindPath3D(Point3D start, Point3D end)
        {
            throw new NotSupportedException();
        }

        protected virtual uint CalculateHeuristc(Point2D start, Point2D end, Point2D p)
        {
            return (uint)(Math.Abs(p.X - end.X) + Math.Abs(p.Y - end.Y));
        }
    }
}
