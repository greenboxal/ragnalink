﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core.Algorithms
{
    public interface IMap
    {
        int Width { get; }
        int Height { get; }

        uint GetCellWeight2D(Point2D point);
        uint GetCellWeight3D(Point3D point);
    }
}
