﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace RagnaLink.Core
{
    public class LinkConfig
    {
        public IPAddress ListenIP { get; set; }
        public int ListenPortBase { get; set; }

        public IPAddress ExternalIP { get; set; }

        public IPEndPoint LoginEP { get; set; }

        public LinkConfig()
        {
            ListenIP = IPAddress.Loopback;
            ListenPortBase = 6901;

            ExternalIP = IPAddress.Loopback;
            //LoginEP = new IPEndPoint(IPAddress.Parse("128.241.92.162"), 6900); // iRO
            //LoginEP = new IPEndPoint(IPAddress.Parse("108.170.31.90"), 6901); // <- My Bot :D
            LoginEP = new IPEndPoint(IPAddress.Parse("96.31.79.42"), 6900); // Dreamer-RO HR
        }
    }
}
