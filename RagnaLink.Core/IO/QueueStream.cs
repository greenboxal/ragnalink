﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RagnaLink.Core.IO
{
    public class QueueStream
    {
        private MemoryStream _data;
        public MemoryStream Data
        {
            get { return _data; }
        }

        public bool Locked { get; set; }

        public bool Empty
        {
            get { return _position == _data.Length; }
        }

        public int Rest
        {
            get { return (int)_data.Length - _position; }
        }

        private int _position;
        public int Position
        {
            get { return _position; }
        }

        private Mutex _sync;
        private ManualResetEvent _crossSync;

        public QueueStream()
        {
            _data = new MemoryStream();
            _sync = new Mutex();
            _crossSync = new ManualResetEvent(true);

            _position = 0;
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            int read;

            _sync.WaitOne();

            _data.Seek(_position, SeekOrigin.Begin);
            read = _data.Read(buffer, offset, count);
            _position += read;

            if (_position > 4096 || Empty)
                Flush(false);

            _sync.ReleaseMutex();

            return read;
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            Write(buffer, offset, count, true);
        }

        public void Write(byte[] buffer, int offset, int count, bool lk)
        {
            if (lk)
                _crossSync.WaitOne();

            _sync.WaitOne();

            _data.Seek(0, SeekOrigin.End);
            _data.Write(buffer, offset, count);

            _sync.ReleaseMutex();
        }

        public void Skip(int size)
        {
            _position += size;
        }

        public void Flush()
        {
            Flush(true);
        }

        private void Flush(bool doLock)
        {
            if (doLock)
                _sync.WaitOne();

            if (_position == _data.Length)
            {
                _data = new MemoryStream();
                _position = 0;
            }
            else
            {
                MemoryStream tmp = new MemoryStream();

                tmp.Write(_data.GetBuffer(), _position, (int)(_data.Length - _position));
                _position = 0;

                _data = tmp;
            }

            if (doLock)
                _sync.ReleaseMutex();
        }

        public byte[] Peek(int p1, int p2)
        {
            byte[] data;

            if (Rest < p1 + p2)
                return null;

            data = new byte[p2];
            Buffer.BlockCopy(_data.GetBuffer(), _position + p1, data, 0, p2);

            return data;
        }

        public void Lock()
        {
            _crossSync.Reset();
            Locked = true;
        }

        public void Unlock()
        {
            _crossSync.Set();
            Locked = false;
        }

        public void Wait()
        {
            _crossSync.WaitOne();
        }
    }
}
