﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

public static class Utils
{
    public static string ReadCString(this BinaryReader br)
    {
        string str = "";

        do
        {
            byte b = br.ReadByte();

            if (b == 0)
                break;

            str += (char)b;
        }
        while (true);

        return str;
    }

    public static string ReadCString(this BinaryReader br, int size)
    {
        int i;
        string str = "";

        for (i = 0; i < size; i++)
        {
            byte b = br.ReadByte();

            if (b == 0)
                break;

            str += (char)b;
        }

        if (i < size)
            br.ReadBytes(size - i - 1);

        return str;
    }

    public static void WriteCString(this BinaryWriter bw, string str, int size)
    {
        for (int i = 0; i < size; i++)
        {
            if (i < str.Length)
                bw.Write((byte)str[i]);
            else
                bw.Write((byte)0);
        }
    }

    public static void EncodePosition(byte[] data, int offset, int x, int y, int dir)
    {
        data[offset + 0] = (byte)(x >> 2);
        data[offset + 1] = (byte)((x << 6) | ((y >> 4) & 0x3f));
        data[offset + 2] = (byte)((y << 4) | (dir & 0xf));
    }

    public static void DecodePosition(byte[] data, int offset, out int x, out int y, out int dir)
    {
        x = ((data[offset + 0] & 0xff) << 2) | (data[offset + 1] >> 6);
        y = ((data[offset + 1] & 0x3f) << 4) | (data[offset + 2] >> 4);
        dir = (data[offset + 2] & 0x0f);
    }

    public static void DecodePosition2(byte[] data, int offset, out int x0, out int y0, out int x1, out int y1, out int sx, out int sy)
    {
        x0 = ((data[offset + 0] & 0xff) << 2) | (data[offset + 1] >> 6);
        y0 = ((data[offset + 1] & 0x3f) << 4) | (data[offset + 2] >> 4);
        x1 = ((data[offset + 2] & 0x0f) << 6) | (data[offset + 3] >> 2);
        y1 = ((data[offset + 3] & 0x03) << 8) | (data[offset + 4] >> 0);
        sx = (data[offset + 3] & 0xF0) >> 4;
        sy = (data[offset + 3] & 0x0F) >> 0;
    }

    public static double EuclideanDistance(double x0, double y0, double x1, double y1)
    {
        return Math.Sqrt((x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1));
    }

    public static int ManhattanDistance(int x0, int y0, int x1, int y1)
    {
        return Math.Abs(x0 - x1) + Math.Abs(y0 - y1);
    }
}
