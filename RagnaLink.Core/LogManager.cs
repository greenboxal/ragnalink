﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RagnaLink.Core
{
    public class LogManager
    {
        public static LogManager Instance { get; set; }

        static LogManager()
        {
            Instance = new LogManager();
        }

        private List<string> _lines;
        public List<string> Lines
        {
            get { return _lines; }
        }

        public string LastLine
        {
            get { return _lines[_lines.Count - 1]; }
            set { _lines[_lines.Count - 1] = value; OnLastChanged(); }
        }

        public event EventHandler LineAdded;
        public event EventHandler LastChanged;

        private LogManager()
        {
            _lines = new List<string>();
        }

        public void AppendLine(string format, params object[] args)
        {
            _lines.Add(string.Format(format, args));
            OnLineAdded();
        }

        private void OnLineAdded()
        {
            if (LineAdded != null)
                LineAdded(this, null);
        }

        private void OnLastChanged()
        {
            if (LastChanged != null)
                LastChanged(this, null);
        }
    }
}
