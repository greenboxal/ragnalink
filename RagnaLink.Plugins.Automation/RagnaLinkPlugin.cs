﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RagnaLink.Core;
using RagnaLink.Core.Algorithms;
using RagnaLink.Core.Plugin;
using RagnaLink.Core.Ragnarok;
using RagnaLink.Core.Ragnarok.Network;

namespace RagnaLink.Plugins.Automation
{
    public class RagnaLinkPlugin : IPlugin, IZoneHandler
    {
        private class PrivateStore
        {
            
        }

        private MapCache _cache;

        public string Name
        {
            get { return "RagnaLink.Plugins.Automation"; }
        }

        public void Initialize()
        {
            _cache = new MapCache();

            LinkManager.Instance.RegisterZoneCommand("move", MoveCommand);
            LinkManager.Instance.RegisterZoneHandler(this);
        }

        public void OnClientStart(ZoneProxyClient client)
        {
            PrivateStore ps = new PrivateStore();

            client.RegisterService(ps);
        }

        public bool OnChatReceived(ZoneProxyClient client, int senderID, string senderName, string text)
        {
            return false;
        }

        public bool OnChatSent(ZoneProxyClient client, string sender, string text)
        {
            return false;
        }

        public bool OnPacketSent(ZoneProxyClient client, ushort id, byte[] data, DateTime time)
        {
            return false;
        }

        public bool OnPacketReceived(ZoneProxyClient client, ushort id, byte[] data, DateTime time)
        {
            return false;
        }

        public bool OnClientWalk(ZoneProxyClient client, string mapname, int x, int y, int tx, int ty, bool teleport)
        {
            LogManager.Instance.AppendLine("Move to {0} {1}-{2} {3}-{4}", mapname, x, tx, y, ty);

            return false;
        }

        private void MoveCommand(ZoneProxyClient client, string sender, string[] args)
        {
            int x = int.Parse(args[0]);
            int y = int.Parse(args[1]);

            MapDescriptor desc = LinkManager.Instance.Maps.GetMap(client.Cookie.StartMap.ToLower());

            client.SendText("Calculating route...");
            Stack<Point> nodes = desc.Finder.FindPath(new Point(client.Cookie.TX, client.Cookie.TY), new Point(x, y));

            if (nodes != null && nodes.Count > 0)
            {
                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                DateTime time = DateTime.Now;
                Point prev = new Point(client.Cookie.X, client.Cookie.Y);

                bw.Write((ushort)0x35f);
                while (nodes.Count > 0)
                {
                    Point node = nodes.Pop();
                    client.SendText("Route: {0}, {1}", node.X, node.Y);
                    Utils.EncodePosition(ms.GetBuffer(), 2, node.X, node.Y, 0);
                    client.SendDataToServer((byte[])ms.GetBuffer().Clone(), 0, 5, time);
                    time = time.AddMilliseconds(300 + client.Cookie.Speed * (int)Utils.EuclideanDistance(prev.X, prev.Y, node.X, node.Y));
                    prev = node;
                }
                client.SendText("YAY! Path!");
            }
            else
                client.SendText("Destination unreachable.");
        }
    }
}
