﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RagnaLink.Core;
using RagnaLink.Core.Plugin;
using RagnaLink.Core.Ragnarok;
using RagnaLink.Core.Ragnarok.Network;

namespace RagnaLink.Plugins.Skills
{
    public class RagnaLinkPlugin : IPlugin, IZoneHandler
    {
        private class PlayerPosition
        {
            public int x, y, dir;
        }

        private class PrivateStore
        {
            public bool backStab;
            public Dictionary<int, PlayerPosition> playerPos;
        }

        public string Name
        {
            get { return "RagnaLink.Plugins.Skills"; }
        }

        public void Initialize()
        {
            LinkManager.Instance.RegisterZoneCommand("skill.backstab", CommandBackstab);
            LinkManager.Instance.RegisterZoneHandler(this);
        }

        public void OnClientStart(ZoneProxyClient client)
        {
            PrivateStore ps = new PrivateStore();

            ps.backStab = true;
            ps.playerPos = new Dictionary<int, PlayerPosition> { };

            client.RegisterService(ps);
        }

        public bool OnChatReceived(ZoneProxyClient client, int senderID, string senderName, string text)
        {
            return false;
        }

        public bool OnChatSent(ZoneProxyClient client, string sender, string text)
        {
            return false;
        }

        public bool OnPacketSent(ZoneProxyClient client, ushort id, byte[] data, DateTime time)
        {
            bool block = false;
            PrivateStore ps = client.GetService<PrivateStore>();

            if (id == 0x438)
            {
                int aid = BitConverter.ToInt32(data, 6);
                int skillId = BitConverter.ToInt16(data, 4);
                if (skillId == 212 && ps.backStab)
                {
                    if (ps.playerPos.ContainsKey(aid))
                    {

                        MemoryStream ms = new MemoryStream();
                        BinaryWriter bw = new BinaryWriter(ms);

                        int[] dx = { 0, 1, 1, 1, 0, -1, -1, -1 };
                        int[] dy = { -1, -1, 0, 1, 1, 1, 0, -1 };
                        int x = ps.playerPos[aid].x + dx[ps.playerPos[aid].dir];
                        int y = ps.playerPos[aid].y + dy[ps.playerPos[aid].dir];

                        int d = /*Math.Abs(x + y - client.Cookie.X - client.Cookie.Y) + 1*/ 3;


                        bw.Write((ushort)0x35f);
                        Utils.EncodePosition(ms.GetBuffer(), 2, x, y, ps.playerPos[aid].dir);

                        client.SendDataToServer(ms.GetBuffer(), 0, 5, DateTime.Now);

                        bw.BaseStream.Position = 0;
                        bw.Write((ushort)0x438);
                        bw.Write((ushort)10);
                        bw.Write((ushort)212);
                        bw.Write((int)aid);
                        client.SendDataToServer(ms.GetBuffer(), 0, 10, DateTime.Now.AddMilliseconds(client.Cookie.Speed * d + 10));
                        block = true;
                        client.SendText(DateTime.Now.AddMilliseconds(client.Cookie.Speed * d + 10), "Dir updated, backstabbing now.");
                    }
                    else
                        client.SendText("[Back Stab] No position data for this target.");
                }
            }

            return block;
        }

        private static byte map_calc_dir(int dx, int dy, int x, int y)
        {
            byte dir = 0;

            dx = x - dx;
            dy = y - dy;
            if (dx == 0 && dy == 0)
            {
                //todo: replace 0 to initial source dir
                dir = 0;
            }
            else if (dx >= 0 && dy >= 0)
            {	// upper-right
                if (dx * 2 <= dy) dir = 0;	// up
                else if (dx > dy * 2) dir = 6;	// right
                else dir = 7;	// up-right
            }
            else if (dx >= 0 && dy <= 0)
            {	// lower-right
                if (dx * 2 <= -dy) dir = 4;	// down
                else if (dx > -dy * 2) dir = 6;	// right
                else dir = 5;	// down-right
            }
            else if (dx <= 0 && dy <= 0)
            {	// lower-left
                if (dx * 2 >= dy) dir = 4;	// down
                else if (dx < dy * 2) dir = 2;	// left
                else dir = 3;	// down-left
            }
            else
            {	// upper-left
                if (-dx * 2 <= dy) dir = 0;	// up
                else if (-dx > dy * 2) dir = 2;	// left
                else dir = 1;	// up-left

            }
            return dir;
        }


        public bool OnPacketReceived(ZoneProxyClient client, ushort id, byte[] data, DateTime time)
        {
            PrivateStore ps = client.GetService<PrivateStore>();

            if (id == 0x86)
            {
                int aid = BitConverter.ToInt32(data, 2);
                int x, y, tx, ty, sx, sy;
                Utils.DecodePosition2(data, 6, out x, out y, out tx, out ty, out sx, out sy);

                if (!ps.playerPos.ContainsKey(aid))
                    ps.playerPos.Add(aid, new PlayerPosition());

                PlayerPosition playerPos = ps.playerPos[aid];
                playerPos.x = tx;
                playerPos.y = ty;
                playerPos.dir = map_calc_dir(x, y, tx, ty);

                string dirName = "";
                
                switch (playerPos.dir)
                {
                    case 0: dirName = "up"; break;
                    case 1: dirName = "up-left"; break;
                    case 2: dirName = "left"; break;
                    case 3: dirName = "down-left"; break;
                    case 4: dirName = "down"; break;
                    case 5: dirName = "down-right"; break;
                    case 6: dirName = "right"; break;
                    case 7: dirName = "up-right"; break;
                }

                client.SendText("Object will be facing {0}.", dirName);
            }
            else if (id == 0x88 || id == 0x1ff)
            {
                int aid = BitConverter.ToInt32(data, 2);
                int x = BitConverter.ToInt16(data, 6);
                int y = BitConverter.ToInt16(data, 8);

                if (!ps.playerPos.ContainsKey(aid))
                    ps.playerPos.Add(aid, new PlayerPosition());

                PlayerPosition playerPos = ps.playerPos[aid];
                playerPos.x = x;
                playerPos.y = y;
            }
            else if (id == 0x9c)
            {
                int aid = BitConverter.ToInt32(data, 2);
                int dir = data[8];

                if (!ps.playerPos.ContainsKey(aid))
                    ps.playerPos.Add(aid, new PlayerPosition());

                PlayerPosition playerPos = ps.playerPos[aid];    
                playerPos.dir = dir;

                client.SendText("Object changed direction to {0}.", dir);
            }
            return false;
        }

        public bool OnClientWalk(ZoneProxyClient client, string mapname, int x, int y, int tx, int ty, bool teleport)
        {
            return false;
        }

        private void CommandBackstab(ZoneProxyClient client, string sender, string[] args)
        {
            PrivateStore ps = client.GetService<PrivateStore>();

            ps.backStab = !ps.backStab;
            client.SendText("Backstab enforcement is now {0}.", ps.backStab ? "enabled" : "disabled");
        }
    }
}
