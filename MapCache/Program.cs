﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GRFSharp;
using RagnaLink.Core.Ragnarok;

using RMapCache = RagnaLink.Core.Ragnarok.MapCache;

namespace MapCache
{
    class Program
    {
        private static List<GRF> _grfs;

        private static GRFFile FindFile(GRF grf, string name)
        {
            foreach (GRFFile file in grf.Files)
            {
                if (file.Name == name)
                    return file;
            }

            return null;
        }

        static void Main(string[] args)
        {
            _grfs = new List<GRF>();

            foreach (string grfFile in args)
            {
                GRF grf = new GRF();

                try
                {
                    grf.Open(grfFile);
                }
                catch
                {
                    Console.WriteLine("Failed opening GRF file: {0}", grfFile);
                    goto finalize;
                }

                _grfs.Add(grf);
            }

            RMapCache mc = new RMapCache();

            foreach (GRF grf in _grfs)
            {
                foreach (GRFFile gatFile in grf.Files)
                {
                    if (gatFile.Name.EndsWith(".gat"))
                    {
                        GRFFile rswFile = FindFile(grf, gatFile.Name.Substring(0, gatFile.Name.Length - 4) + ".rsw");
                        byte[] gat = grf.GetDataFromFile(gatFile);
                        byte[] rsw = null;
                        
                        if (rswFile != null)
                            rsw = grf.GetDataFromFile(rswFile);

                        MapData map = new MapData();
                        float waterHeight = 1000000;

                        if (rsw != null)
                            waterHeight = BitConverter.ToSingle(rsw, 166);
                        
                        map.Name = Path.GetFileNameWithoutExtension(gatFile.Name);
                        map.Width = BitConverter.ToInt32(gat, 6);
                        map.Height = BitConverter.ToInt32(gat, 10);
                        map.Cells = new CellTypeWrapper[map.Width * map.Height];

                        Console.WriteLine(map.Name + "...");

                        int off = 14;
                        for (int xy = 0; xy < map.Cells.Length; xy++, off += 20)
                        {
                            float height = waterHeight = BitConverter.ToSingle(gat, off);
                            int type = BitConverter.ToInt32(gat, off + 16);
                            CellType ct = CellType.NonWalkable;

                            if (type == 0 && waterHeight != 1000000 && height > waterHeight)
                                type = 3;

                            switch (type)
                            {
                                case 0: ct = CellType.Walkable; break;
                                case 1: ct = CellType.NonWalkable; break;
                                case 2: ct = CellType.Walkable; break;
                                case 3: ct = CellType.Water; break;
                                case 4: ct = CellType.Walkable; break;
                                case 5: ct = CellType.NonWalkable; break;
                                case 6: ct = CellType.Walkable; break;
                            }

                            map.Cells[xy].Type = ct;
                        }

                        mc.Maps.Add(map.Name, map);
                    }
                }
            }

            FileStream fs = new FileStream("mapcache.dat", FileMode.Create);
            mc.Save(fs);
            fs.Close();

            finalize:
            foreach (GRF grf in _grfs)
            {
                grf.Close();
            }

            Console.WriteLine("Press any key to continue.");
            Console.ReadLine();
        }
    }
}
