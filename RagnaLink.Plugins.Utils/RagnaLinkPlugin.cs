﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RagnaLink.Core;
using RagnaLink.Core.Plugin;
using RagnaLink.Core.Ragnarok;
using RagnaLink.Core.Ragnarok.Network;

namespace RagnaLink.Plugins.Utils
{
    public class RagnaLinkPlugin : IPlugin, IZoneHandler
    {
        private class PrivateStore
        {
            public string myName;
            public long pingTotal;
            public long pingLast;
            public long pingCount;
            public Stopwatch pingCounter;
            public bool noBlindness;
            public int noOrc;
        }

        public string Name
        {
            get { return "RagnaLink.Plugins.Utils"; }
        }

        public void Initialize()
        {
            LinkManager.Instance.RegisterZoneCommand("ping", CommandPing);
            LinkManager.Instance.RegisterZoneCommand("dice", CommandDice);
            LinkManager.Instance.RegisterZoneCommand("quit", CommandQuit);
            LinkManager.Instance.RegisterZoneCommand("noblind", CommandNoBlindness);
            LinkManager.Instance.RegisterZoneCommand("noorc", CommandNoOrc);
            LinkManager.Instance.RegisterZoneHandler(this);
        }

		public void OnClientStart(ZoneProxyClient client)
        {
            PrivateStore ps = new PrivateStore();

            ps.myName = "";
            ps.pingCounter = new Stopwatch();
            ps.noBlindness = false;
            ps.noOrc = 0;

            client.RegisterService(ps);
        }

        public bool OnChatReceived(ZoneProxyClient client, int senderID, string senderName, string text)
        {
            PrivateStore ps = client.GetService<PrivateStore>();

            if (senderID == client.ConnectionID && senderName == ps.myName)
            {
                ps.pingCounter.Stop();

                ps.pingLast = ps.pingCounter.ElapsedMilliseconds;
                ps.pingTotal += ps.pingLast;
                ps.pingCount++;

                ps.pingCounter.Reset();
            }

            return false;
        }

        public bool OnChatSent(ZoneProxyClient client, string sender, string text)
        {
            PrivateStore ps = client.GetService<PrivateStore>();

            ps.myName = sender;
            ps.pingCounter.Start();

            return false;
        }

        public bool OnPacketSent(ZoneProxyClient client, ushort id, byte[] data, DateTime time)
        {
            return false;
        }

        public bool OnPacketReceived(ZoneProxyClient client, ushort id, byte[] data, DateTime time)
        {
            PrivateStore ps = client.GetService<PrivateStore>();

            if (id == 0x229 || id == 0x119)
            {
                MemoryStream ms = new MemoryStream(data);
                BinaryReader br = new BinaryReader(ms);
                BinaryWriter bw = new BinaryWriter(ms);

                br.BaseStream.Position = 2;
                int aid = br.ReadInt32();
                ushort option1 = br.ReadUInt16();
                ushort option2 = br.ReadUInt16();
                uint option = (id == 0x229) ? br.ReadUInt32() : br.ReadUInt16();
                
                if (aid == client.ConnectionID)
                {
                    if ((option2 & 0x10) != 0 && ps.noBlindness)
                    {
                        client.SendText("You are blind.");
                        option2 = (ushort)(option2 & ~0x10);
                    }
                    if ((option & 0x800) != 0 && ps.noOrc != 0)
                    {
                        client.SendText("Your head looks like an orc.");
                        option = (uint)(option & ~0x800);
                    }
                }
                else if ((option & 0x800) != 0 && ps.noOrc == 2)
                {
                    option = (uint)(option & ~0x800);
                }
                
                bw.BaseStream.Position = 6;
                bw.Write((ushort)option1);
                bw.Write((ushort)option2);
                bw.Write((id == 0x229) ? (uint)option : (ushort)option);
            }

            return false;
        }

        public bool OnClientWalk(ZoneProxyClient client, string mapname, int x, int y, int tx, int ty, bool teleport)
        {
            return false;
        }

        private void CommandPing(ZoneProxyClient client, string sender, string[] args)
        {
            PrivateStore ps = client.GetService<PrivateStore>();

            if (ps.pingCount > 0)
            {
                client.SendText("Average Ping: {0}ms", ps.pingTotal / ps.pingCount);
                client.SendText("Last Ping: {0}ms", ps.pingLast);
            }
            else
            {
                client.SendText("No ping data to display.");
            }
        }

        private void CommandDice(ZoneProxyClient client, string sender, string[] args)
        {
            int dice;
            if (String.IsNullOrEmpty(args[0]) || !int.TryParse(args[0], out dice) || dice < 1 || dice > 6)
            {
                client.SendText("Usage: {0}dice <1-6>", LinkManager.CommandPrefix);
                return;
            }
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);

            bw.Write((ushort)0xbf);
            bw.Write((byte)(58 + dice - 1)); // E_DICE1 = 58

            bw.Flush();
            client.SendDataToServer(ms, DateTime.Now);
        }

        private void CommandQuit(ZoneProxyClient client, string sender, string[] args)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            byte type = 0;

            if (args.Length > 0)
                type = byte.Parse(args[0]);

            bw.Write((ushort)0x18a);
            bw.Write((byte)type);
            
            bw.Flush();
            client.SendDataToServer(ms, DateTime.Now);
        }

        private void CommandNoBlindness(ZoneProxyClient client, string sender, string[] args)
        {
            PrivateStore ps = client.GetService<PrivateStore>();

            ps.noBlindness = !ps.noBlindness;
            if (ps.noBlindness)
            {
                client.SendText("Blindness effect won't be displayed.");
            }
            else
            {
                client.SendText("Blindness effect will be displayed.");
            }
        }

        private void CommandNoOrc(ZoneProxyClient client, string sender, string[] args)
        {
            PrivateStore ps = client.GetService<PrivateStore>();
            int newNoOrc = (ps.noOrc + 1) % 3;

            if (args.Length >= 1)
            {
                if (String.Compare(args[0], "off", true) == 0)
                    newNoOrc = 0;
                else if (String.Compare(args[0], "me", true) == 0)
                    newNoOrc = 1;
                else if (String.Compare(args[0], "all", true) == 0)
                    newNoOrc = 2;
                else
                {
                    client.SendText("Usage: {0}noorc <off/me/all>", LinkManager.CommandPrefix);
                    return;
                }  
            }

            ps.noOrc = newNoOrc;
            switch (ps.noOrc)
            {
                case 0:
                    client.SendText("Orc heads will be displayed.");
                    break;
                case 1:
                    client.SendText("Orc heads on you won't be displayed.");
                    break;
                case 2:
                    client.SendText("Orc heads won't be displayed.");
                    break;
            }
        }
    }
}
