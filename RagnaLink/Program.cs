﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using RagnaLink.Core;

namespace RagnaLink
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            LogManager.Instance.LineAdded += Instance_LineAdded;
            LogManager.Instance.LastChanged += Instance_LastChanged;

            Console.WriteLine("RagnaLink 1.0");
            Console.WriteLine("Copyright (c) 2012 RagnaLink Team");

            LinkManager.Instance.Start();

            Thread.Sleep(Timeout.Infinite);
        }

        private static void Instance_LineAdded(object sender, EventArgs e)
        {
            Console.Write("\n" + LogManager.Instance.LastLine);
        }

        private static void Instance_LastChanged(object sender, EventArgs e)
        {
            int top = Console.CursorTop;

            Console.SetCursorPosition(0, top);
            for (int i = 0; i < Console.WindowWidth; i++)
                Console.Write(" ");
            Console.SetCursorPosition(0, top);
            Console.Write(LogManager.Instance.LastLine);
        }
    }
}
